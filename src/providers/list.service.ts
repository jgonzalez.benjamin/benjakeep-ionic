import { Injectable } from "@angular/core";
import { List } from '../models/index';

@Injectable()

export class ListService {

    lists: List[] = [];

    constructor(){
        
        this.loadStorage();

       /*  const list1 = new List('find infinity stones');
        const list2 = new List('heroes to beat');

        this.lists.push(list1,list2);

        console.log(this.lists); */
        
    }
    addList( list:List ){
        this.lists.push(list);
        this.saveStorage();
    }
    saveStorage(){
        localStorage.setItem('lists', JSON.stringify(this.lists));
    }
    loadStorage(){
        if( localStorage.getItem('lists') ){
            this.lists = JSON.parse(localStorage.getItem('lists'));
        }else{
            this.lists = [];
        }
    }
    deleteList(list:List){

        this.lists = this.lists.filter( data => {
            return data.id != list.id
        });

        this.saveStorage();

    }


}