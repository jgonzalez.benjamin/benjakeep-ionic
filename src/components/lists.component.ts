import { Component, Input } from '@angular/core'
import { ListService } from '../providers/list.service';
import { List } from '../models';
import { AddPage } from '../pages/add/add';
import { NavController, AlertController, ItemSliding } from 'ionic-angular';
@Component({
    selector: 'app-lists',
    templateUrl: 'lists.component.html'
})
export class ListsComponent {

    @Input() finished:boolean = false;

    constructor( private _listService:ListService, private navCtrl:NavController, private alertCtrl:AlertController ){
        
    }
    deleteList(list:List){
        this._listService.deleteList(list);
    }
    
    listSelected(list:List){
        this.navCtrl.push(AddPage,{title: list.title,list: list});
        
    }
    updateList( list:List, slidingItem: ItemSliding){

        slidingItem.close();

        const alert =  this.alertCtrl.create({
            title: 'Update name of list',
            message: 'Enter a name for this list',
            inputs: [
                {
                    name: 'title',
                    placeholder: 'Title',
                    value: list.title
                }
            ],
            buttons: [
                {text: 'Cancel'},
                {
                    text: 'Update',
                    handler: data=>{
                        if (data.title.length === 0) {
                            return;
                        }
                        list.title = data.title;
                        this._listService.saveStorage();
                    }
                }
            ]
        });
        alert.present();
    }

}