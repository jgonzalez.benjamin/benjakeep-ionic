import { NgModule } from '@angular/core';
import { FilterCompletePipe } from './filter-complete/filter-complete';
@NgModule({
	declarations: [FilterCompletePipe],
	imports: [],
	exports: [FilterCompletePipe]
})
export class PipesModule {}
