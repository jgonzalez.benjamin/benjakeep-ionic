import { Component } from "@angular/core";
import { ListService } from '../../providers/list.service';
import { List } from "../../models";
import { NavController, AlertController } from "ionic-angular";
import { AddPage } from '../add/add';

@Component({
    selector: 'page-pending',
    templateUrl: 'pending.component.html'
})

export class PendingPage {

    constructor( public _listService:ListService, private navCtrl: NavController, 
        private alertCtrl:AlertController ){

    }
    newList(){
    
        const alert = this.alertCtrl.create({
            title: 'New List',
            message: 'Enter a name for this new list',
            inputs: [
                {
                    name: 'title',
                    placeholder: 'Title'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data =>{console.log('Cancel clicked')}
                        
                    
                },
                {
                    text:'Save',
                    handler: data=>{
                        if(data.title.length === 0){
                            return;
                        }else{
                            this.navCtrl.push(AddPage,{
                                title: data.title
                            });
                            console.log('Save clicked');
                        }
                    }
                    
                }
            ]
        });
        alert.present();

    } 

    

}