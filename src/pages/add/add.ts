import { Component } from '@angular/core';
import { ListService } from '../../providers/list.service';
import { List, ListItem } from '../../models';
import { NavParams } from 'ionic-angular';




@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
  list:List;
  itemName:string = '';

  constructor( private _listService:ListService, private navParams:NavParams) {
   
    const title = this.navParams.get('title');
    
    if(this.navParams.get('list')){
      this.list = this.navParams.get('list');
    }else{
      this.list = new List(title);
      this._listService.addList( this.list );
    }

  }
  addItem(){
    if(this.itemName.length === 0 ){
      return;
    }
    const newItem = new ListItem(this.itemName);

    this.list.items.push(newItem);
    this.itemName = '';
    this._listService.saveStorage();
  }
  updateTask( item:ListItem){
    item.complete = !item.complete;
  
    const pending = this.list.items.filter( itemData=>{
        return !itemData.complete;
    }).length;

    if( pending === 0 ){
      this.list.finish = true;
      this.list.finished = new Date();
    }else{
      this.list.finish = false;
      this.list.finished = null;
    }

    this._listService.saveStorage();
  }
  delteItem(i:number){
    this.list.items.splice(i, 1);
    this._listService.saveStorage();
    
  }

}
